class Envideo < ActiveRecord::Base
  belongs_to :user
  before_save :set_unic
  
   scope :unic, -> {where(unic: true)} 
  
  def set_unic
    self.video = self.video[0..10]
    (self.unic = false) if Envideo.where(video: self.video).first
    
    return true  
  end
  
end
